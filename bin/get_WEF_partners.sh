#!/bin/sh

curl https://www.weforum.org/partners \
| perl -n -E 'if (m{div\s+class="js-partner-app"\s+data-state="([^"]+)"}) { $_=$1; s{\&quot;}{"}g; say $_ }' \
| jq -r '.partnerTypes[].organizations[].title'
