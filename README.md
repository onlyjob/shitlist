## List of disgraceful organisations to avoid

This is a curated list of unethical disgraceful organisations with whom it
is shameful to associate, do business or work for.  
Avoiding their services and products is important from ethical prospective.

Every company should work _for_, and _with_ community of people.
Unfortunately the following organisations betrayed us.

Nominations and corrections are welcome.


### aggressive de-platforming and suppression of free speech

  * ADL.org (Anti-Defamation League)
  * Adobe
  * Amazon
  * Arm
  * Change.org
  * City National Bank of Florida
  * Dropbox
  * ECCU (Christian Banking)
  * Facebook
  * GoDaddy
  * GoFundMe
  * Google / YouTube
  * Instagram
  * Intel
  * LinkedIn
  * Mailchimp
  * Mastercard
  * Microsoft
  * Mozilla
  * NBT Bank
  * Patreon
  * PayPal
  * Reddit
  * Salesforce
  * Spotify
  * Squarespace
  * Stripe
  * Truepic
  * Twitter
  * Vimeo
  * Visa
  * web.com (Network Solutions | Register.com | 1ShoppingCart | Name Secure | SnapNames )


### lies, mis-information, bias, propaganda

  * ABC News(abc.net.au)
  * Al Jazeera
  * Avaaz.org
  * BBC
  * Bloomberg
  * CBS News
  * Chicago Tribune
  * CNBC
  * CNN
  * Crikey (.com.au)
  * FactCheck.org
  * Forbes
  * Huffpost
  * LeadStories
  * MIT
  * MSNBC
  * news.com.au
  * New York Times
  * PolitiFact
  * ResearchSquare
  * RollingStone (.com)
  * Quartz (qz.com)
  * quillette.com
  * Skeptical Raptor (.com)
  * Science-Based Medicine (.org)
  * Scientific American (.com)
  * Snopes
  * Southern Poverty Law Center (splcenter.org)
  * The Atlantic
  * The Conversation (.com)
  * The Daily Beast
  * The Lincoln Project
  * The Sydney Morning Herald (smh.com.au)
  * The Sun (.co.uk)
  * The Washington Post
  * TIME
  * USAToday
  * Vice
  * Vox
  * [Wikipedia](ref/wikipedia.md)


### censorship

  * ResearchGate
  * Spotify


### Merchants of death, anti-human agenda, corruption

  * Accenture
  * AstraZeneca
  * Australian Psychological Society
  * Bill & Melinda Gates Foundation
  * CDC (and their partner organisations https://www.cdcfoundation.org/partner-list/corporations)
    * 360 thinc, ltd.
    * 3M
    * 3M Pharmaceuticals
    * AB Biodisk
    * Abbott Ireland
    * Abbott Laboratories
    * AbbVie Inc.
    * AB SCIEX
    * Academic Press
    * Accenture
    * ACE Construction Group
    * Acme Cargo Express Singapore
    * ActionSprout
    * Adis International
    * Adobe
    * Advanstar Communications, Inc.
    * Advantage Healthcare, Inc.
    * Aetna, Inc.
    * Affimedix, Inc.
    * AFLAC
    * AG Communications, LLC
    * Agilent Technologies
    * AGL Networks LLC
    * AGL Resources, Inc.
    * AirTran Airways
    * AkzoNobel N.V.
    * Alaska Center for Pediatrics
    * Alere North America, Inc.
    * Allergan, Plc
    * AlphaGraphics
    * Alston + Bird, LLP
    * American Petroleum Institute
    * Amgen Inc.
    * Analytic Services Inc.
    * Annar Diagnostica
    * Anthem Blue Cross and Blue Shield
    * Anthem, Inc.
    * Apatel.com, Inc.
    * Appalachian Power
    * Applied BioCode, Inc.
    * Applied Biosystems
    * Arch Chemicals, Inc.
    * Ark Sciences, Inc.
    * Arrowsmith Consulting, LLC
    * Ascend Media
    * ASI Holdings Inc.
    * Astellas Pharma US, Inc.
    * Astra Merck, Inc.
    * AstraZeneca PLC
    * Atkinson & Associates, LLC
    * Atlanta Coca-Cola Bottling Company
    * Atlanta Magazine
    * AT&T Corp.
    * AT&T Georgia
    * Autodesk, Inc.
    * Aventis Pasteur
    * Aventis Pasteur Limited
    * Aventis Pharmaceuticals
    * AVNET Inc
    * Barlovento LLC
    * BASF Corporation
    * Batavia Biosciences
    * Bavarian Nordic, Inc.
    * Baxter International Inc.
    * Bayer AG
    * Bayer Corporation
    * Bayer U.S. LLC
    * BD (Becton, Dickinson and Company)
    * BearingPoint, Inc.
    * Beckman Coulter, Inc.
    * BellSouth Corporation
    * BellSouth Telecommunications, LLC
    * Bethpage Federal Credit Union
    * Bijur Lubricating Corporation
    * bioCSL
    * Biogen MA Inc.
    * Biohit Laboratory Services
    * bioMerieux, sa
    * BioPort Corporation
    * Bio-Rad Laboratories, Inc.
    * BioReference Laboratories, Inc.
    * BioTechnical Communications, Inc.
    * Blackwell Science Ltd.
    * BMH Holdings LLC
    * Boditech Med Inc.
    * Boehringer Ingelheim USA Corporation
    * Boehringer Mannheim Corporation
    * Bond, Schoeneck & King, PLLC
    * Boopies Gifts
    * Booz Allen Hamilton Inc.
    * Boston Field & Focus, Inc.
    * Brakeley Briscoe, Inc.
    * Branford Festival Corporation
    * Bristol-Myers Squibb Company
    * Browning-Ferris Industries, Inc.
    * Bunge Limited
    * Byotrol Inc.
    * Cambridge University Press
    * Cargill, Inc.
    * Caribbean Travel Medicine Clinic
    * Celanese
    * Celerant Consulting, Inc.
    * Centocor Ortho Biotech, Inc.
    * Century 21 Today
    * Cepheid
    * Cerilliant Corporation
    * Chem-Aquascience, Inc.
    * Chemux Bioscience, Inc.
    * Chesapeake Surveys
    * Chiron Corporation
    * Chromesystems Instruments & Chemicals GmbH
    * CIBA Vision Corporation
    * CIBC World Markets Corporation
    * Cisco Systems, Inc.
    * Citi
    * Clarivate Analytics
    * Clinical Care Options
    * Clinical Pathology Laboratories, Inc.
    * CNN/Time Warner
    * Columbia Management
    * Columbus Auto Body Works, Inc
    * Commonwealth Radiology, PC
    * ConocoPhillips Company
    * Consensus Communications, Inc
    * Contello Towers III, Inc.
    * Contemporary Forums
    * Cook Insurance Agency, Inc.
    * Cooney Waters Group, Inc.
    * Cortlandt Communications, Inc.
    * Costco
    * Covance
    * Covance Market Access Service
    * Coventry Health Care of Georgia
    * Covidien
    * Coxe Curry & Associates, Inc.
    * Craft Technologies, Inc.
    * Crawford Media Services, Inc.
    * CRC Industries, Inc.
    * CSIRO Livestock Industries
    * Cubist Pharmaceuticals, Inc.
    * Cummins Inc.
    * Current Drugs Ltd.
    * Current Medicine Group, LLC
    * Custom Ink
    * Custom Packaging Services, Inc.
    * CVR Associates, Inc.
    * CVS/caremark Annual Giving Campaign
    * CYTYC Corporation
    * Dade MicroScan Inc.
    * D.A. Hughes & Associates
    * Daiichi Pharmaceutical Company, Ltd.
    * Darren's Designs
    * Data Management Processing, Inc.
    * David Vance & Associates, Inc.
    * DaVita Mission Viejo
    * Decision Resources, Inc.
    * Dell Inc.
    * Deloitte Consulting LLP
    * Deloitte Services LP
    * Delta Air Lines, Inc.
    * Delta Development Group, Inc.
    * Delta Staffing Services, Inc.
    * Devlin Properties LLC
    * Devries Public Relations
    * DEY, L.P.
    * D.H. Griffin Companies
    * Diagnostics Biochem Canada Inc.
    * Dialysis Clinic, Inc.
    * DiaSorin Inc.
    * DIAsource Immunoassays
    * Diazyme Laboratories
    * Disney Worldwide Services, Inc.
    * Dodge & Cox Funds
    * DSM Nutritional Products, Inc.
    * DST Systems, Inc.
    * Eastern Mountain Sports
    * Eastman Kodak Company
    * Easton Associates LLC
    * Eastview Veterinary Clinic, P.C.
    * Ecolab
    * Edelman
    * Educational Management Group
    * EIS, Inc
    * Eli Lilly and Company
    * Elsevier Inc.
    * Emergent BioSolutions Inc.
    * Endocrine Sciences Inc.
    * Entertainment Media Ventures
    * Environmental Disciplines, Inc
    * Environmental Systems Research Institute, Inc
    * ESA Biosciences, Inc.
    * Esoterix, Inc
    * ESSRX
    * E.T. Enterprises
    * Eurofins Biomnis
    * Eventbrite
    * Evercore Partners Services East LLC
    * Exel Inc.
    * Exponent
    * ExxonMobil
    * Facebook
    * Fallon Medica LLC
    * FanBox
    * Ferraiuoli LLC
    * Fidelity Investments
    * Fleishman-Hillard, Inc.
    * FMC Technologies, Inc.
    * Ford Motor Company
    * Four Seasons Environmental, Inc
    * Fujirebio Diagnostics, Inc
    * Fujirebio Inc.
    * Future Drugs, LTD
    * Ganister Fields Architects
    * GDF Suez Energy North America, Inc.
    * GE Aviation
    * GE Energy
    * GE Medical Systems
    * Genentech, Inc.
    * General Electric Company
    * General Mills, Inc.
    * General Motors Corporation
    * Gen-Probe, Incorporated
    * Genzyme Corporation
    * Georgia-Pacific Corporation
    * Georgia-Pacific Professional
    * Georgia Power
    * GF Health Products, Inc.
    * Gilead Sciences, Inc.
    * GlaxoSmithKline Biologicals S.A.
    * Global Blood Therapeutics, Inc.
    * Global Good Fund I LLC.
    * Global, Inc.
    * GOJO Industries, Inc.
    * Google Inc.
    * Granotec Chile S.A.
    * Greenberg Traurig, LLP
    * Gustav Heningburg Associates, Inc.
    * Hager Companies
    * Hamad Medical Corporation
    * Hampton Inn Hotel
    * Harold's Printing Company
    * Harris Teeter
    * Haymarket Media, Inc.
    * Haymarket Medical
    * H.B. Sherman Trap, Inc.
    * HCA, Inc.
    * Health Advances LLC
    * Healthcare Performance Consulting, Inc.
    * Healthcomp, Inc.
    * Health Education Technologies
    * Heartland Assays LLC
    * Henry Schein, Inc.
    * Hexagon Nutrition Pvt. Ltd.
    * H & H Consulting Partners, LLC
    * Hilton Hotels Corporation
    * Hoffmann-La Roche Inc.
    * Hogan Lovells US LLP
    * Holder Construction Company
    * Holland America Line
    * Hologic Inc.
    * Horizon Communications
    * Horizon Properties Corporation
    * IASIS Healthcare
    * IBM Corporation
    * IBM Employee Services Center
    * ICN Pharmaceuticals Inc.
    * Idenix Pharmaceuticals
    * Ihc Health Services Inc
    * IISES, LLC
    * Image Information, Inc.
    * IMC
    * Imedex, Inc.
    * IMG
    * Immundiagnostik AG
    * Immunodiagnostic Sytems Inc.
    * Immunotech S.A.S.
    * Impact Communications, Inc.
    * ImpactRx, Inc.
    * ImQuest BioSciences
    * Incstar Corporation
    * Independent Charities of America
    * Indusco Distribution of America, Inc.
    * Industrial Metal Powders
    * Infectious Disease Consultants PC
    * Integrated Communications Corp.
    * Intellectual Concepts LLC
    * Intellectual Ventures Management, LLC
    * Inter-American Development Bank
    * International Association of Operative Millers
    * International Medical Publishing, Inc.
    * Intervet International B.V.
    * inVentiv Health Clincial Lab Inc.
    * Invitrogen, Life Technologies
    * Iodine Global Network
    * IQ Solutions
    * Jackson Associates, Inc.
    * Jalonso Group Inc.
    * Janssen Biotech, Inc.
    * Janssen Global Services, LLC
    * Janssen Scientific Affairs, LLC
    * Janssen Therapeutics
    * Jealco International, Inc. (West)
    * Johnson & Johnson
    * Johnson & Johnson Pediatric Institute, LLC
    * Johnson & Johnson Pharmaceutical Services, LLC
    * John Wiley & Sons, Inc.
    * Jones & Askew, LLP
    * Jupitor Corporation USA
    * JVC Professional Products Company
    * JWT
    * Kadmon Pharmaceuticals, LLC
    * Kaiser Permanente
    * Kaiser Permanente Northern California Division of Research
    * Kalik & Associates, Inc.
    * Kedrion Biopharma Inc.
    * Kendall Strategies
    * Kerricook Construction, Inc.
    * Kilmek, Kolodney & Casale, P.C.
    * Kimberly-Clark Corporation
    * King & Spalding LLP
    * KIPHS, Inc.
    * Knoll AG
    * Kohl's Corporation
    * KPMG International
    * Kris International, LLC
    * Laboratory Alliance of Central New York
    * Laboratory Corporation of America Holdings
    * Laburnum Diagnostic Imaging Assoc.
    * LadDawn
    * Land Yacht Harbor of Melbourne Airstream Park
    * Law Offices of Edwin Marger, LLC
    * Liberty Mutual Insurance
    * Life Technologies Corporation
    * LigoCyte Pharmaceuticals
    * LinksPoint
    * Linn Energy
    * Lippincott Williams & Wilkins
    * Lipsey Mountain Spring Water
    * Lonza Group Ltd.
    * Luminex Corporation
    * Luminex Molecular Diagnostics, Inc.
    * Lupin Pharmaceuticals, Inc.
    * Lypro Biosciences, Inc.
    * Magellan Biosciences, Inc.
    * MailChimp
    * Mallinckrodt Group Inc.
    * Marc O'Brien Agency, Inc.
    * MarketVision, Inc.
    * McDonald's Corporation
    * McGraw-Hill Education LLC
    * Medentech Ltd.
    * Medical Economics Company, Inc.
    * Medical Information Systems, Inc.
    * Medical Marketing Research International Ltd.
    * Medical Marketing Studies U.S. Inc.
    * MediMedia Managed Markets, L.L.C.
    * Medimmune, Inc.
    * MediTech Media Ltd.
    * MedPanel, Inc.
    * Medrad Inc.
    * Medscape, LLC
    * Mellon Private Wealth Management
    * Members Give
    * Mercer (US) Inc.
    * Merck
    * Merck for Mothers
    * Merck Partnership for Giving
    * Metro Foods
    * Metro Logics, Inc.
    * MGK Insect Control Solutions
    * Mickey Leland National Urban Air Toxics Research Center
    * Microbide Limited
    * Microgenics Corporation
    * Micromass UK Ltd
    * Microsoft Corporation
    * Milbank, Tweed, Hadley & McCloy LLP
    * Moelis & Company
    * Molnlycke Health Care, LLC
    * Mondrian Investment Partners (U.S.), Inc.
    * Morgan Keegan & Co., Inc.
    * Mo's Pizza
    * Motorola Inc.
    * Murphy-Moore Properties
    * N2 Qualitative Marketing Research
    * NanoEnTek Inc.
    * Nanosphere, Inc.
    * National Football League
    * Navkar bio-chem
    * NDC Health
    * New England American College of Sports Medicine
    * New England Research Institutes, Inc.
    * New & Improved, LLC
    * New York Society of Infectious Diseases, Inc.
    * Nexstar Farmaceutica, S.A.
    * Nike, Inc.
    * North American Vaccine, Inc.
    * Northrop Grumman Corporation
    * Novartis AG
    * Novartis Investments S.A.R.L.
    * Novartis Pharmaceuticals Corporation
    * Novartis Vaccines and Diagnostics, Inc.
    * Novavax, Inc.
    * Novo Nordisk Inc.
    * OCC North America, Inc.
    * Ogilvy & Mather
    * Oliver Wyman
    * OnStar Corporation
    * Onyx Pharmaceuticals, Inc.
    * Opko Diagnostics, LLC
    * OraSure Technologies, Inc.
    * ORAVAX, Inc.
    * Ortho-Clinical Diagnostics
    * Ortho-McNeil Pharmaceutical, Inc.
    * Orvis Atlanta Store Staff
    * Oxford Immunotec Ltd.
    * Oxford University Press USA
    * Oxoid Ltd.
    * Paint World, Inc.
    * PAREXEL Medical Marketing Services
    * Pathcon Laboratories
    * Pathology Associates Medical Laboratories
    * Patterson Belknap Webb & Tyler LLP
    * PayPal
    * PD Bros
    * Penn Schoen & Berland Accociates LLC
    * Penn Treaty Kennel Club
    * Perera V. Chiron Corporation
    * PerkinElmer, Inc.
    * Pfizer Inc.
    * P&G
    * PGS Tech Corp.
    * Pharmacia Corporation
    * Pharmavite LLC
    * PHFE Management Solutions
    * P.H. Moore Anesthesia Services, Inc.
    * PlowShare Group Inc.
    * Porter Novelli
    * PPD Development, LP
    * Practakal LLC
    * Premier, Inc.
    * Preparis Inc.
    * Princeton Media Associates, Inc.
    * Production Transport, Inc.
    * Prospect Associates, Ltd.
    * Proteus On-Demand Facilities
    * Public Health Management Corp.
    * Pursuant
    * QIAGEN Corporation
    * Qualcomm
    * Qualigen, Inc.
    * Quest Diagnostics
    * Quidel Corporation
    * RADCO Chemical Solutions, Inc.
    * Rasmus Real Estate Group, Inc.
    * RB Health (US) LLC
    * R-Biopharm Inc.
    * Reckitt Benckiser, Inc.
    * Regional Health Services of Howard County
    * Remel Inc.
    * ReproSource
    * Research in Motion
    * Rexall Sundown, Inc.
    * Richard Bowers & Co.
    * Robert Michael Communications, Inc.
    * Roche Diagnostics Corporation
    * Roche Laboratories, Inc.
    * Roche Molecular Systems, Inc.
    * RockTenn Company
    * Rockwell Collins
    * Rogers Medical Education Network (USA) Inc.
    * Rollins, Inc.
    * Run It Up Productions, Inc.
    * Sacoor Medical Group, Europe
    * Safeway Inc.
    * Sage Publications, Inc.
    * San Diego Gas & Electric
    * Sands Point Nursing Home
    * Sanofi-Aventis
    * Sanofi Pasteur, Inc.
    * Satair USA, Inc.
    * Schering Plough Corporation
    * Science Applications International Corporation
    * Scientific-Atlanta, Inc.
    * Scientific Therapeutics Info., Inc.
    * SCIEX LLC
    * S.C. Johnson & Son, Inc.
    * Seaport Global Holdings LLC
    * SerOptix
    * Shaklee Corporation
    * Shalin Financial Services, Inc.
    * Share Your Share, Inc.
    * Shoreview Nursing Home
    * Shure, Incorporated
    * Siemens Energy & Automation, Inc.
    * Siemens Healthcare Diagnostics, Inc.
    * Siemens USA
    * Sikorsky Aircraft Corporation
    * Simon Kucher & Partners Strategy & Marketing Consultants LLC
    * Simon Property Group
    * Skanska USA Building, Inc.
    * Skippy Musket & Co.
    * Slack, Inc.
    * SmithKline Beecham Pharmaceuticals
    * Social & Scientific Systems, Inc.
    * Solutions
    * Solvay Pharmaceuticals, Inc.
    * Sonnenschein Nath & Rosenthal
    * Southeast Research, Inc.
    * Southern Company
    * Southern Company Services, Inc.
    * Spectrum Brands
    * Spirit AeroSystems
    * Springer SBM BV
    * Springer Science+Business Media LLC
    * Sprint
    * SRA International, Inc.
    * Staples, Inc.
    * Starbucks Corporation
    * Starfish Associates Limited
    * State Farm Insurance Companies
    * St. Elizabeth Healthcare
    * Stiefel Laboratories, Inc.
    * Stratton Travel Management
    * Subjective-i Research
    * Subterracom Wireless Solutions, LLC
    * Summit Chemical Company
    * SunTrust Bank
    * SunTrust One Pledge Campaign
    * SureWerx USA Inc.
    * Symbol Technologies, Inc.
    * Synageva BioPharma Corp.
    * Synermed
    * Takeda Pharmaceutical Company Limited
    * Takeda Pharmaceuticals U.S.A., Inc.
    * Target Corporation
    * Taylor and Francis Group, LLC
    * Taylor Technology, Inc.
    * The American Academy of Allergy, Asthma & Immunology
    * The Analytica Group
    * The Atlanta Journal Constitution
    * The Bank of Commerce
    * The Charles Schwab Corporation
    * The Clorox Company
    * The Coca-Cola Company
    * The Father's Love Consulting and Investment Services Inc.
    * The Frankel Group, Inc.
    * The Home Depot
    * The James W. Down Company, Inc.
    * The Law Offices of Kevin J. McDonough
    * The Leader Publishing Group, Inc.
    * The McGraw-Hill Companies
    * The Medical Letter, Inc.
    * The Quaker Oats Company
    * The Quigley Corporation
    * Theranos
    * Thermo Fisher Scientific, Inc.
    * ThermoForma, Inc.
    * The Segal Company
    * The Sherwin-Williams Company
    * The Stormont Companies LLC
    * Thompson, Ventulett, Stainback & Associates, Inc.
    * Thomson Healthcare Inc.
    * Thomson Reuters
    * Thoroughbred Research Group Inc.
    * Tibra Trading America
    * Times Bank Ltd.
    * T-Mobile USA, Inc.
    * TMS BioScience Labs
    * Tosoh Bioscience, Inc.
    * Tosoh Corporation
    * ToucanEd, Inc.
    * Toyota Motor Sales, U.S.A., Inc.
    * Transtria
    * Trellis Bioscience, Inc.
    * Trellis RSV Holdings, Inc.
    * Tricore Reference Laboratories
    * Tri-State Gastroenterology
    * Trojan Brand Condoms
    * True North Communications, Inc.
    * T-Shirt Mojo
    * Turner Broadcasting System, Inc.
    * Turner Construction Company
    * Turner Learning
    * UBS AG
    * Underwriters Laboratories, Inc.
    * United Industries Corporation
    * United Parcel Service
    * Universal Stabilization Technologies, Inc.
    * University Hospital Gent
    * UpToDate, Inc.
    * USF Medical Services Support Corporation
    * USGlobalSat, Inc.
    * U.S. Magnesium
    * Valeant Pharmaceuticals International
    * Ventria Bioscience
    * Verizon Communications
    * Vertex Pharmaceuticals, Incorporated
    * Vestergaard Frandsen SA
    * Victor G. Polizos, M.D., P.C.
    * Vipelderly Limited
    * ViralED, LLC
    * Viropharma Incorporated
    * ViroStat, Inc.
    * Vista Research
    * Visual Education Corporation
    * Visual Response LTD
    * Viva Entertainment, LLC
    * Voxiva
    * Wachtell, Lipton, Rosen & Katz
    * Walgreen Company
    * Walmart Stores, Inc.
    * Waters Corporation
    * Waters Ireland LTD
    * WebMD, LLC
    * Westat
    * Wirthlin Worldwide
    * Wolters Kluwer Health
    * Workday, Inc.
    * World Health Communications, Inc.
    * Wyeth
    * Wyeth Consumer Healthcare
    * Wyeth Global Pharmaceuticals
    * Wyeth Laboratories
    * Wyeth Pharmaceuticals
    * Xcel Energy
    * XCellAssay, LLC
    * YourCause, LLC
    * YRC Worldwide
    * YUM! Brands, Inc.
    * Zeus Scientific, Inc.
    * Zhejiang Disigns Diagnostics
    * Zila Pharmaceuticals, Inc.
    * ZRT Laboratory, LLC
    * Zurich American Insurance Company
  * CoviPass
  * Gavi
  * GlaxoSmithKline (GSK)
  * IDEO
  * Immunize Canada (immunize.ca)
  * Merck
  * Moderna
  * OECD (Organisation for Economic Co-operation and Development)
  * Pfizer
  * Planned Parenthood (plannedparenthood.org)
  * Rockefeller Foundation
  * Sanofi Pasteur Limited
  * Seqirus - a CSL Limited Company (seqirus.com)
  * TGA (.gov.au)
  * The Hastings Center
  * United Nations (UN)
  * Uber Health
  * U.S. Chamber of Commerce Foundation
  * [WHO](ref/WHO.md) (and their contributors https://www.who.int/about/funding/contributors)
    * Bill & Melinda Gates Foundation
    * GAVI Alliance
    * UN Office for the Coordination of Humanitarian Affairs (UNOCHA)
    * European Commission
    * Rotary International
    * National Philanthropic Trust
    * UN Central Emergency Response
    * World Bank
  * World Economic Forum (and their partners https://www.weforum.org/partners)
    * 360 DigiTech
    * 58 Daojia
    * AARP
    * ABB
    * Abbott Laboratories
    * ABN AMRO
    * Absa Group
    * Accenture
    * Acciona
    * Ackermans & van Haaren
    * Adani Group
    * Adecco Group
    * Aditya Birla Group
    * Adobe Systems
    * Advantage Partners
    * Aecon Group
    * Aegon
    * African Development Bank Group
    * African Rainbow Minerals
    * Ageas
    * Agility
    * AgroAmerica
    * AIB Group
    * AIG
    * Airbus
    * Air Liquide
    * Airport Authority Hong Kong
    * Aker
    * Alcon
    * Algebris Investments
    * AlGihaz
    * Alibaba Group
    * AlixPartners
    * Allianz
    * Allied Bank
    * Al Nowais Investments
    * Al-Othman Holding
    * Alrosa
    * Al Sakr for Food Industries
    * Alshaya Group
    * Al-Ula
    * Amanat Holdings
    * Amara Raja Group
    * Amazon
    * Amcor
    * América Móvil
    * American Heart Association
    * American Tower
    * AmeriSourceBergen
    * AMTD
    * Analog Devices
    * Angelicoussis Group
    * Anglo American
    * Anglobal
    * AngloGold Ashanti
    * Angola Cables
    * Anheuser-Busch InBev
    * Ankorgaz
    * Antin Infrastructure Partners
    * Antwerp Port Authority
    * Apis Partners
    * A.P. Møller-Maersk
    * Apollo Hospitals Enterprise
    * Apollo Tyres
    * Appen
    * Apple
    * ArcelorMittal
    * Ariston Thermo
    * Arm
    * Aroundtown
    * Arup Group
    * Asia World Group
    * ASO Corporation
    * Aster DM Healthcare
    * AstraZeneca
    * ASYAD
    * Aurec Capital Group
    * Aurora Innovation
    * Autodesk
    * Automatic Data Processing (ADP)
    * Automation Alley
    * Avast Software
    * AXA
    * Axtria
    * BAE Systems
    * Bahrain Economic Development Board
    * Bain & Company
    * Bajaj Auto
    * Baker Hughes
    * Baker McKenzie
    * Bakrie & Brothers
    * Banco Bradesco
    * Banco BTG Pactual
    * Banco Nacional de Panamá (Banconal)
    * Banco Safra Brasil
    * Banco Santander
    * Bangchak
    * Bangkok Bank
    * Bank Julius Baer
    * Bank Leumi Le-Israel
    * Bank Lombard Odier & Co.
    * Bank Mandiri (Persero)
    * Bank of America
    * Bank of Qingdao
    * Barclays
    * BASF
    * Bayer
    * BB Energy
    * BBVA
    * BC Energy Investments
    * Beximco Group
    * Bharat Forge
    * Bharti Airtel
    * Bill & Melinda Gates Foundation
    * Biogen
    * Bittrex Global
    * BlackRock
    * Blackstone Group
    * Bloomage International
    * Bloomberg
    * BMO Financial Group
    * BNY Mellon
    * Boeing
    * BoodaiCorp
    * Borealis
    * Boston Consulting Group (BCG)
    * bp
    * BRANDi and Companies
    * Breca
    * Bridgewater Associates
    * Brightstar Capital Partners
    * Broadridge Financial Solutions
    * Brookfield Asset Management
    * Brunswick Group
    * BTS Group
    * Byco Petroleum Pakistan
    * BYJU'S
    * ByteDance
    * Caisse de dépôt et placement du Québec (CDPQ)
    * Canica
    * Cantor Fitzgerald
    * Capgemini
    * Capricorn Investment Group
    * Cargill
    * Carlsberg Group
    * Carson Cumberbatch
    * Cedar Holdings Group
    * Centene
    * Check Point Software Technologies
    * Chengdu Xingcheng Investment Group
    * Chevron
    * Chiba Bank
    * China Bohai Bank
    * China Construction Bank
    * China Energy Investment
    * China Huaneng Group Co., Ltd.
    * China Merchants Group
    * China Railway Group (CREC)
    * China Southern Power Grid
    * China UnionPay
    * CH. Karnchang Public Company Limited (CK)
    * Circle
    * Cisco
    * Citi
    * CITIC Capital
    * CJ
    * Clariant
    * Clayton, Dubilier & Rice
    * Clifford Chance
    * Cloudflare
    * CLS Bank International
    * Codewise
    * Cognizant
    * Commercial International Bank (CIB)
    * Commercial League Healthcare & Insurance Group
    * Compagnie Financière Tradition
    * ComTrade Group
    * Condé Nast
    * Consolidated Contractors Company (CCC)
    * Contemporary Amperex Technology (CATL)
    * Coursera
    * Covestro
    * CP Group
    * CPP Investments
    * Credit Suisse
    * Crescent Enterprises
    * Crescent Petroleum
    * CVC Capital Partners (Luxembourg)
    * CVS Health
    * CZD Holding
    * Dabur India
    * D. A. Consortium
    * Dairy Management
    * Daiwa Securities Group
    * Dalberg
    * Dalian Bingshan Group
    * Dalian Commodity Exchange
    * Dalian Hi-Think Computer Technology
    * Dalmia Bharat Group
    * DAMAC International Limited
    * Dana Gas
    * Danfoss
    * Dassault Systèmes
    * Dataminr
    * DataRobot
    * DatVietVAC
    * Dawood Hercules
    * DBL Group
    * Dell Technologies
    * Deloitte
    * Dentsu
    * Depository Trust & Clearing (DTCC)
    * Descon
    * Desktop Metal
    * Deutsche Bank
    * Deutsche Post DHL
    * Development Bank of Japan (DBJ)
    * Development Bank of Southern Africa
    * Diligent
    * Disa
    * Discovery
    * Disrupt Digital
    * DNB
    * Dogan Group of Companies
    * Dow
    * DP World
    * DST Global
    * DTEK
    * Dubai Electricity and Water Authority
    * DXC Technology
    * East Capital
    * Ecolab
    * Ecopetrol
    * Edelman
    * Egon Zehnder International
    * Eisai
    * Eletrobras
    * Elite Education Group
    * Emirates Group
    * Enel
    * ENGIE Group
    * Eni
    * EnQuest
    * Envision Group
    * EQT
    * Equifax
    * Equinor
    * Ericsson
    * ER-Telecom
    * Eurasian Resources Group (ERG)
    * Euroclear
    * European Bank for Reconstruction and Development (EBRD)
    * European Investment Bank
    * Export Credit Insurance Corporation of South Africa (ECIC)
    * Exxaro
    * EY
    * Ezaki Glico
    * Facebook
    * Far East Consortium
    * Far East Holding Group
    * Fédération Internationale de l'Automobile (FIA)
    * Fidelity International
    * FinAccel
    * FirstRand
    * Flex
    * Flottweg
    * Fluor
    * Fohoway (Tianjin) Pharmaceutical
    * Forcepoint LLC
    * Fosun International
    * Fractal Analytics
    * Frigoglass
    * FTI Consulting
    * Fubon Financial Holding
    * Fujitsu
    * Fullerton Health
    * Galp
    * Garrett Motion
    * Gartner
    * GE
    * GEA
    * Geminicorp Recycling
    * General Atlantic Partners
    * Generali
    * Genetron Health
    * GEP
    * Getinge Group
    * GFG Alliance
    * GIC
    * Ginkgo Bioworks
    * Giti Group
    * Glencore International
    * Global Asset Capital
    * GLOBIS
    * GMR Infrastructure
    * Godrej Industries
    * Goldman Sachs
    * Google
    * Grab
    * Grain Management
    * Green Delta Insurance
    * Greenko
    * Greiner
    * Grundfos
    * Grupo Mega
    * GS Group
    * Guangzhou Automobile Group
    * Guccio Gucci
    * Guggenheim Partners
    * Guidance
    * GuideWell
    * Gulf International Bank (GIB)
    * Gulfstream Aerospace
    * Gunvor Group
    * Hackensack Meridian Health
    * Haier Group
    * Hanwha Group
    * HCC
    * HCL Technologies
    * Heidrick & Struggles
    * HEINEKEN
    * Hengchang
    * Henkel
    * Henry Schein
    * Hepsiburada
    * Hero Group
    * Hess Corporation
    * Hewlett Packard Enterprise
    * Hikma Pharmaceuticals
    * Hillspire
    * Hitachi
    * H & M (Hennes & Mauritz)
    * Holman Enterprises
    * Hologic
    * Home Instead
    * Honda Motor
    * Honeywell
    * Hong Kong Exchanges and Clearing (HKEX)
    * Hong Kong Science and Technology Parks (HKSTP)
    * HP
    * HPS Investment Partners
    * HP Trust
    * HSBC
    * HTC
    * Huawei Technologies
    * Hubert Burda Media
    * Hunt Consolidated Energy
    * Huobi Global
    * Hyundai Motor
    * Iberdrola
    * IBM
    * IDEO
    * IFFCO
    * Ihlas Holding
    * IHS
    * IHS Markit
    * II-VI
    * Illumina
    * illycaffè
    * IN4.OS US
    * Indigo Agriculture
    * Indika Energy
    * Indofood
    * Indorama Ventures
    * Indus Group
    * Industrial and Commercial Bank of China (ICBC)
    * Industrial Development Corporation of South Africa
    * Infosys
    * ING Group
    * Ingka Group (IKEA)
    * Inmarsat Global
    * Instinctif Partners
    * Integral Petroleum
    * Intel
    * Inter-American Development Bank
    * Intercorp
    * Intermountain Healthcare
    * Interpublic Group
    * Interros
    * Intesa Sanpaolo
    * Invesco
    * Investcorp
    * Investec
    * Investment Promotion Agency Qatar (IPA Qatar)
    * Ipsos
    * IQVIA
    * Iron Mountain Information Management
    * Islamic Development Bank
    * Itaú Unibanco
    * ITOCHU
    * Jacobs
    * Japan Bank for International Cooperation (JBIC)
    * JBS
    * JD.com
    * Jefferson Health
    * JERA
    * JERA
    * Jerónimo Martins
    * Jetex Flight Support
    * JLL
    * JM Eagle
    * John Keells Holdings
    * Johnson Controls
    * Johnson & Johnson
    * Johnson Matthey
    * JPMorgan Chase & Co.
    * J. Safra Group
    * JS Group
    * Jubilant Bhartia Group
    * Juffali and Brothers
    * Jumeirah Group
    * Kaiser Permanente
    * Kaizen Institute
    * Kale Holding
    * Kcap Holdings
    * Kearney
    * Keidanren (Japan Business Federation)
    * Kikkoman
    * King & Wood Mallesons
    * Kirin Holdings
    * Koç Holding
    * Kohlberg Kravis Roberts & Co.
    * KOIS
    * Körber
    * KPMG
    * Kudelski Group
    * Kuwaiti Danish Dairy (KDD)
    * Lab49
    * Lakestar Advisors
    * Lawson
    * Lazard
    * LeasePlan
    * Lennar
    * Lenzing Group
    * LG Chem
    * LGT Group
    * Liberty Global
    * Limak Holding
    * LinkedIn
    * Lippo Group
    * Lloyds Banking Group
    * Lord, Abbett & Co.
    * Louis Dreyfus Company
    * LRN
    * LUG
    * Luksic Group
    * Lulu Group International
    * LVMH Moët Hennessy - Louis Vuitton
    * LyondellBasell
    * Maekyung Media Group
    * Magic Leap
    * Mahindra Group
    * Majid Al Futtaim
    * Manchester United
    * ManpowerGroup
    * Manulife
    * Mari Petroleum
    * Marsh McLennan
    * Martin Dow
    * Mary Kay
    * Mastercard
    * Mayo Clinic
    * McKinsey & Company
    * Medtronic
    * Mengniu Group
    * Merck
    * Mercuria Energy Group
    * Meridiam
    * Meridian
    * Micron Technology
    * Microsoft
    * Midea Group
    * Midis Group
    * Millicom
    * MIND ID
    * MiSK
    * Mitsubishi Chemical Holdings
    * Mitsubishi Corporation
    * Mitsubishi Heavy Industries
    * Mitsui Chemicals
    * Mitsui & Co.
    * Mitsui O.S.K. Lines
    * Mizuho Financial Group
    * Mjalli Investment Group
    * MKS (Switzerland)
    * Moderna
    * Moelis & Company
    * Monaco Economic Board
    * Morgan Lewis
    * Morgan Stanley
    * Mori Building Co.
    * Motive Partners
    * Mott MacDonald
    * Mozilla
    * MSD
    * MTN Group
    * MTR
    * Mubadala
    * MUFG Bank
    * Nasdaq
    * Naspers
    * Natixis
    * Natura &Co
    * Naturgy
    * Natwest Group
    * NBCUniversal
    * NEC
    * Nedbank Group
    * Nestlé
    * Neusoft
    * Newmont
    * New York Times
    * NEXUS
    * Nielsen
    * Nikkei
    * Nishith Desai Associates
    * NN Group
    * Nokia
    * Nomura Holdings
    * Norilsk Nickel
    * North Island
    * Novartis
    * Novatek
    * Novolipetsk Steel (NLMK)
    * Novo Nordisk Foundation
    * NR Instant Produce
    * NTPC
    * Nutrien
    * NV Investments
    * NYSE
    * o9 Solutions
    * Oando
    * Occidental
    * Oerlikon
    * Oisix ra daichi Inc.
    * Olayan Financing Group
    * Old Mutual
    * OMERS
    * OMINVEST
    * Ontario Teachers' Pension Plan
    * Open Society Foundations
    * Optel Group
    * Orano
    * Orano
    * Organic and Beyond
    * Orion Heritage
    * Orkla
    * Ørsted
    * Otsuka America Pharmaceutical
    * OVG Real Estate
    * Pacific Invest (PIMCO)
    * PAG
    * Palantir Technologies
    * Palo Alto Networks
    * PALO IT Group
    * Pampa Energia
    * Parsable
    * Pathfinder Group
    * PayPal
    * Paytm
    * PensionDanmark
    * Pentland Group
    * PepsiCo
    * Perfect World
    * Permira Advisers
    * Petroleo Brasileiro - PETROBRAS
    * PETRONAS (Petroliam Nasional)
    * Pfizer
    * Pharco
    * Phongsavanh Group
    * PhosAgro
    * Pictet Group
    * Pladis Foods
    * Power Dekor Group
    * PPF
    * Procter & Gamble
    * Providence
    * Prudential
    * PSA International
    * PSP Investments
    * PTC
    * PTT
    * Public Institution for Social Security (PIFSS)
    * Publicis Groupe
    * Puma Energy
    * Puravankara
    * PwC
    * Qatar Development Bank
    * Qatar Financial Centre (QFC)
    * Qatar Investment Authority
    * QIAGEN
    * QI Group
    * Qualcomm
    * Quest Diagnostics
    * Quexco
    * Rabobank
    * Rakuten Group
    * Ralph Lauren
    * Ralph Lauren
    * Randstad
    * RBC (Royal Bank of Canada)
    * Recruit Holdings
    * Refinitiv
    * Reliance Industries
    * Remixpoint
    * RenaissanceRe Holdings
    * ReNew Power
    * Repsol
    * RGE
    * Rio Tinto
    * Ripple
    * Risk Insights
    * RMZ
    * Robert Bosch
    * Roche
    * Rockwell Automation
    * ROHTO Pharmaceutical
    * Roland Berger
    * Royal DSM
    * Royal Dutch Shell
    * Royal Philips
    * Royal Schiphol Group
    * Royal Vopak
    * Russell Reynolds Associates
    * Russian Direct Investment Fund
    * RUSS-INVEST
    * RWE
    * S4Capital
    * Saipem
    * Salesforce
    * Salesforce
    * Sanofi
    * Sany Heavy Industry
    * SAP
    * Saudi Aramco
    * Saudi Basic Industries (SABIC)
    * Saudi Industrial Development Fund
    * Saudi Telecom
    * Sberbank
    * Schlumberger
    * Schneider Electric
    * Schur Flexibles Group
    * Sea
    * Sealed Air
    * SEDCO Holding
    * SEEK Group
    * Sekunjalo Group
    * Sempra
    * Seplat Petroleum
    * Septodont
    * Sequoia Capital Group
    * Serum Institute of India
    * Severstal
    * SG Holdings
    * Sharjah Investment and Development Authority - Shurooq
    * Siam Cement Group (SCG)
    * Sibur
    * SICPA
    * Siemens
    * Siemens Energy
    * Siemens Healthineers
    * Signify
    * Sincerity International Group
    * Singapore Exchange (SGX)
    * Sinochem Group
    * Sino Group
    * SITE
    * SIX Group
    * SK Group
    * Skolkovo Foundation
    * SkyBridge Capital
    * Smart-Holding
    * Snam
    * SNC-Lavalin Group
    * SOCAR (State Oil Company of the Azerbaijan Republic)
    * Sociedad de Fomento Fabril (SOFOFA)
    * Softbank Group
    * Solvay
    * Sompo Holdings
    * Sonangol
    * Sony
    * S&P Global
    * Splunk
    * Standard Bank Group
    * Standard Chartered Bank
    * Stanley Black & Decker
    * State Bank of India
    * State Grid Corporation of China
    * State Oil Fund of the Republic of Azerbaijan (SOFAZ)
    * State Street
    * Stem
    * Stena
    * SUEZ
    * Sumitomo Corporation
    * Sumitomo Mitsui Financial Group (SMFG)
    * Suncor Energy
    * Sun Pharmaceutical Industries
    * Suntory Holdings
    * Sutherland Global Services
    * Swarovski Foundation
    * SWIFT
    * Swire Pacific
    * Swiss Re
    * Syngenta
    * Takeda Pharmaceutical
    * Tamer Group
    * Tata Consultancy Services
    * Tata Sons
    * Tatneft
    * TD Bank Group
    * Team8
    * Technogym
    * Teck Resources
    * Teladoc Health
    * Telenor Group
    * Temasek
    * Temenos
    * Tencent Holdings
    * Teneo
    * Tetra Pak International
    * The Carlyle Group
    * The Coca-Cola Company
    * The Commons Project
    * The LEGO Brand Group
    * Thermo Fisher Scientific
    * Tianjin House Construction Development Group
    * Tianjin Jinbin Development Co.
    * Tianjin TEDA Construction Group
    * TIBCO Software
    * Tillman Global Holdings
    * TIME
    * Tokio Marine
    * Tokopedia
    * TotalEnergies
    * Toyota Motor Corporation
    * TPG
    * Tradeshift
    * Trafigura
    * Trane Technologies
    * Transcosmos
    * Transport Corporation of India
    * Trident
    * Troila Technology
    * Turkey Wealth Fund
    * Turkish Employers Association of Metal Industries (MESS)
    * TÜV SÜD
    * Twilio
    * Tyson Foods
    * Uber Technologies
    * UBS
    * Udacity
    * UL
    * Umicore
    * Unilever
    * Union of Myanmar Federation of Chambers of Commerce and Industry (UMFCCI)
    * Unipol Gruppo
    * Unison Capital
    * Unitel
    * Unlimint
    * UPL
    * UPS
    * USM
    * Vale
    * Vattenfall
    * VdA Legal Partners
    * Verisk Analytics
    * Verizon Communications
    * Vestas
    * VICE Media Group
    * Vietcombank
    * Viking Global Investors
    * Vingroup
    * Visa
    * Vista Equity Partners
    * Vital Capital Fund
    * Vito Energy and Investment
    * VMware
    * Volkswagen Group
    * Volvo Group
    * Voyager Space Holdings
    * VTB Bank
    * Walmart
    * Wellcome Trust
    * Welspun Group
    * Wesfarmers
    * Western Digital
    * Western Union
    * Williams-Sonoma
    * Willis Towers Watson
    * Wipro
    * Wood
    * Workday
    * WorldBridge
    * WorldQuant
    * WPP
    * Xiaomi
    * Xylem
    * Yahoo
    * Yara International
    * Yellowwoods
    * Yidu Tech
    * YMCA of the US
    * Yuanfudao
    * Yuexiu Enterprises
    * Zahid Group
    * Zenith Bank
    * ZF Group
    * Zhongguancun Development Group
    * Zilingo
    * Zoom
    * Zurich Insurance Group
    * Zymergen


### Cashless

  * Woolworths


### Discrimination and segregation

  * David Jones (.com)
  * IBM
  * JB Hi-Fi (jbhifi.com.au)
  * Kmart (.com.au)
  * nab (.com.au)
  * Qantas
  * Red Hat
  * Singapore Airlines
  * The Good Guys (.com.au)


### Proliferation and perpetration of proprietary software and/or patent trolling

  * Microsoft
  * Apple


## Racism/sexism

  * Ycombinator
  * Sass (sass-lang.com)

---

<small>
This list is inspired by the following community maintained resources:

  * https://github.com/privacytools/privacytools.io
</small>
