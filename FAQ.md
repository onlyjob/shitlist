### List is getting too long. Why not list only good organisations instead?

To endorse organisations, a much more thorough research is required.  
Normally malicious organisations should be an exception, not a rule.  
Perhaps it is reasonable to presume innocence by default and only note
organisations that were exposed for/by misconduct.
